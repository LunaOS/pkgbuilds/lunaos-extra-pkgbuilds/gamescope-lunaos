_pkgname=gamescope
pkgname=gamescope-lunaos
pkgver=3.16.1
pkgrel=3
pkgdesc='SteamOS session compositing window manager with added Bazzite patches'
arch=(x86_64)
url=https://github.com/ValveSoftware/gamescope
license=(BSD-2-Clause BSD-3-Clause LicenseRef-Reshade)
depends=(
    gcc-libs
    glibc
    glm
    hwdata
    lcms2
    libavif
    libcap.so
    libdecor
    libdrm
    libinput
    libpipewire-0.3.so
    libx11
    libxcb
    libxcomposite
    libxdamage
    libxext
    libxfixes
    libxkbcommon
    libxmu
    libxrender
    libxres
    libxtst
    libxxf86vm
    luajit
    seatd
    sdl2
    vulkan-icd-loader
    wayland
    xcb-util-wm
    xcb-util-errors
    xorg-server-xwayland
)
makedepends=(
    benchmark
    cmake
    git
    glslang
    meson
    ninja
    vulkan-headers
    wayland-protocols
)

conflicts=("gamescope-lunaos-nvidia" "gamescope")
provides=('gamescope-lunaos-nvidia' "gamescope")
replaces=('gamescope-lunaos-nvidia')

install=$pkgname.install

_tag=4da5e4a37560f9b3c85af2679330f9ec292c8ee1

source=(
  git+https://github.com/ValveSoftware/gamescope.git#tag=${_tag}
  git+https://github.com/Joshua-Ashton/wlroots.git
  git+https://gitlab.freedesktop.org/emersion/libliftoff.git
  git+https://github.com/Joshua-Ashton/vkroots.git
  git+https://gitlab.freedesktop.org/emersion/libdisplay-info.git
  git+https://github.com/ValveSoftware/openvr.git
  git+https://github.com/Joshua-Ashton/reshade.git
  git+https://github.com/Joshua-Ashton/GamescopeShaders.git#tag=v0.1
  git+https://github.com/KhronosGroup/SPIRV-Headers.git
  0001-cstdint.patch
  handheld.patch
  1548.patch
)

b2sums=('35960b5d33604f9649004cf0aa32470349111ec4f41d1750f1fb0525ca4acff070e7874454c3fbebc8f3d00b80f5529c4e80b8cd71dcb3e8ae0967bc3c1c6181'
        'SKIP'
        'SKIP'
        'SKIP'
        'SKIP'
        'SKIP'
        'SKIP'
        'ca268553bc3dacb5bd19553702cd454ea78ed97ab39d4397c5abf9a27d32633b63e0f7f7bf567b56066e6ecd979275330e629ba202a6d7721f0cd8166cd110dd'
        'SKIP'
        '3291e5fd0c3332c9615a65157d9a683d8c3b952b29b6c5a65e0d56e1ed772b4a7bd1f625d9301e67cace6080859156ba2d9876a332ec9e48ed95d34bb241a228'
        '81d8a274ba9b9130560e8e8930dd49700b064a47e9ad54be529296f1db766bd4c16fd1e5ffa18c5d3f0dc0175de1977bbca42f77c16cb3dbb5f12c51f961a575'
        'ca2aeb6c4e0ba35d5d3887e3601f0e031f69ff076d0f725af9f66a45be101922d863898307603b0df086984a5a5d9adb43f58ac95373fe4ebb87ddf8079024b2')

prepare() {

  cd "$srcdir/$_pkgname"
  meson subprojects download

  for submodule in src/reshade subprojects/{libdisplay-info,libliftoff,openvr,vkroots,wlroots} thirdparty/SPIRV-Headers; do
    git submodule init ${submodule}
    git config submodule.${submodule}.url ../${submodule##*/}
  done
  git -c protocol.file.allow=always submodule update

  # apply Bazzite patches from
  # https://github.com/ublue-os/bazzite/blob/main/spec_files/gamescope/gamescope.spec
  local -a patches
  patches=($(printf '%s\n' "${source[@]}" | grep '.patch'))
  patches=("${patches[@]%%::*}")
  patches=("${patches[@]##*/}")

  if (( ${#patches[@]} != 0 )); then
    for patch in "${patches[@]}"; do
        msg2 "Applying patch $patch..."
        patch -d "$srcdir/$_pkgname" -p1 -i "$srcdir/$patch"
    done
  fi
}

build() {
  arch-meson gamescope build \
    --auto-features=enabled \
    -Dpipewire=enabled \
    -Dinput_emulation=enabled \
    -Ddrm_backend=enabled \
    -Drt_cap=enabled \
    -Davif_screenshots=enabled \
    -Dsdl2_backend=enabled \
    -Dforce_fallback_for=glm,stb,libdisplay-info,libliftoff,vkroots,wlroots
  meson compile -C build
}

package() {

  install -d "$pkgdir"/usr/share/gamescope/reshade
  cp -r "$srcdir"/GamescopeShaders/* "$pkgdir"/usr/share/gamescope/reshade/
  chmod -R 655 "$pkgdir"/usr/share/gamescope

  DESTDIR="${pkgdir}" meson install -C build \
    --skip-subprojects
  install -Dm 644 gamescope/LICENSE -t "${pkgdir}"/usr/share/licenses/gamescope/
}
